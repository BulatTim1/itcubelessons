package com.example.testactivity.Drawing;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

import com.example.testactivity.R;

public class MyCanvas extends View {

    MyCanvas(Context context) {
        super(context);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        Paint p = new Paint();
        canvas.drawARGB(255, 0, 255, 255);
        invalidate();
    }

}
