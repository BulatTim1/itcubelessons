package com.example.testactivity.MyDB;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.testactivity.R;

import java.util.ArrayList;
import java.util.List;

public class DBActivity extends AppCompatActivity {

    static MyOpenHelper dbHelper;
    SQLiteDatabase db;
    EditText l, p;
    Button e;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_d_b);
        dbHelper = new MyOpenHelper(this, "myTestDB", null, 1);
        db = dbHelper.getReadableDatabase();
        l = findViewById(R.id.loginDB);
        p = findViewById(R.id.passDB);
        e = findViewById(R.id.enterDB);
        e.setOnClickListener(v->{
            if (l.getText().toString().equals("") || p.getText().toString().equals("")){
                Toast.makeText(getApplicationContext(), "Поля не полные!", Toast.LENGTH_SHORT).show();
            } else {
                Cursor cursor = db.rawQuery("SELECT * FROM user WHERE (email = ? AND pass = ?)", new String[]{l.getText().toString(), p.getText().toString()});
                if (cursor.getCount() > 0){
                    Intent i = new Intent(DBActivity.this, Books.class);
                    startActivity(i);
                } else {
                    Toast.makeText(getApplicationContext(), "ТАКОГО ПОЛЬЗОВАТЕЛЯ НЕТ", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


}