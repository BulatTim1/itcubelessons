package com.example.testactivity.MyDB;

import android.app.Application;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testactivity.Adapters.MyCustomAdapter;
import com.example.testactivity.R;

import java.util.ArrayList;

public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.MyAdapter> {

    ArrayList<Book> arr = new ArrayList<>();

    BooksAdapter(ArrayList<Book> arr){
        this.arr = arr;
    }

    public class MyAdapter extends RecyclerView.ViewHolder{

        TextView price, name, category;
        CardView bookCard;

        public MyAdapter(@NonNull View itemView) {
            super(itemView);
            price = itemView.findViewById(R.id.bookPriceText);
            name = itemView.findViewById(R.id.bookNameText);
            category = itemView.findViewById(R.id.bookCategoryText);
            bookCard = itemView.findViewById(R.id.bookCard);
        }
    }

    @NonNull
    @Override
    public MyAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_item, parent, false);
        BooksAdapter.MyAdapter myAdapter = new BooksAdapter.MyAdapter(v);
        return myAdapter;
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter holder, int position) {
        holder.category.setText(arr.get(position).category);
        holder.name.setText(arr.get(position).name);
        holder.price.setText(String.valueOf(arr.get(position).price));
        holder.bookCard.setOnClickListener(v -> {
            Toast.makeText(v.getContext(), String.valueOf(position), Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public int getItemCount() {
        return arr.size();
    }



}
