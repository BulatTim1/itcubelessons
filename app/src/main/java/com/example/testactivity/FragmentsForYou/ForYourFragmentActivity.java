package com.example.testactivity.FragmentsForYou;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.example.testactivity.R;

import pl.droidsonroids.gif.GifImageView;

public class ForYourFragmentActivity extends AppCompatActivity {

    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_for_your_fragment);
        btn = findViewById(R.id.openFragmentBtn);
        btn.setOnClickListener(v -> {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            FragmentForYou ffy = new FragmentForYou();
            ft.replace(R.id.place_for_fragments, ffy);
            ft.commit();
        });
    }
}