package com.example.testactivity.FragmentsForYou;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.testactivity.R;

public class FragmentForYou extends Fragment {

    Button btn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_for_you, container, false);
        btn = v.findViewById(R.id.FragmentBtn);
        btn.setOnClickListener(v1 -> {
            Toast.makeText(getContext(), "Я ТОАСТ из Фрагмента", Toast.LENGTH_SHORT).show();
        });
        return v;
    }
}