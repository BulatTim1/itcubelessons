package com.example.testactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import pl.droidsonroids.gif.GifImageView;

public class MainActivity extends AppCompatActivity {
    Button solveBtn;
    EditText koefA;
    TextView ansText;
    GifImageView giv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        solveBtn = findViewById(R.id.solveBtn);
        koefA = findViewById(R.id.koefAEdit);
        ansText = findViewById(R.id.answerText);
        solveBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (koefA.getText().toString().equals("")) {

                    Toast.makeText(MainActivity.this,
                            "Вы не заполнили поля!",
                            Toast.LENGTH_LONG)
                            .show();
                } else {
                    ansText.setText(koefA.getText().toString());
                    //из строки в число
                    Double a = Double.parseDouble(koefA.getText().toString());
                    //из числа в строку
                    ansText.setText(String.valueOf(a));
                }
            }
        });
        giv = findViewById(R.id.someGif);
        Timer t = new Timer(0, 30);
        //t.start();
        giv.setActivated(false);
    }
    class Timer extends CountDownTimer{

        public Timer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if (!giv.isActivated()){
                giv.setActivated(true);
            }
        }

        @Override
        public void onFinish() {

        }
    }
}