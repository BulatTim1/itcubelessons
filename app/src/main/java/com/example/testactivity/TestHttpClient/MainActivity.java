package com.example.testactivity.TestHttpClient;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.testactivity.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    String url = "https://koyashkay.tmweb.ru/testFile3s.php";
    OkHttpClient client;
    Button btn;
    EditText login, pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        client = new OkHttpClient();

        login = findViewById(R.id.loginTestEdit);
        pass = findViewById(R.id.passTestEdit);

        btn = findViewById(R.id.getInfo);
        btn.setOnClickListener(v -> {
            new OkHttpHelper().execute("registr", "email", "pass", "secret");
        });

    }

    class OkHttpHelper extends AsyncTask<String, Void, Void>{

        public FormBody createBody(String[] type){
            FormBody result = null;
            switch (type[0]) {
                case "registr":
                    result = new FormBody.Builder()
                            .add("code", type[0])
                            .add("email", type[1])
                            .add("pass", type[2])
                            .add("secret", type[3])
                            .build();
                    break;
                case "":
                    break;
            }
            return result;
        }

        @Override
        protected Void doInBackground(String[] objects) {
            Request request = new Request.Builder()
                    .url(url)
                    .post(createBody(objects))
                    .build();
            try (Response response = client.newCall(request).execute()){
                String s = response.body().string();
                JSONObject js = new JSONObject(s);
                Log.d("MYTAG", js.getString("status"));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if (js.getString("status").equals("success")){
                                    Toast.makeText(MainActivity.this, js.getString("secret"), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(MainActivity.this, "Не верный логин или пароль", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}