package com.example.testactivity.LogInDir;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.testactivity.R;

public class LoginActivity extends AppCompatActivity {

    Button checkBtn;
    TextView resultText;
    EditText login, pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);
        checkBtn = findViewById(R.id.checkButton);
        resultText = findViewById(R.id.resultTextView);
        login = findViewById(R.id.login1EditText);
        pass = findViewById(R.id.passwordEditText);

        checkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (login.getText().toString().equals("admin") && pass.getText().toString().equals("pass")){
                    resultText.setText("УСПЕХ");
                    resultText.setBackgroundColor(Color.GREEN);
                } else {
                    resultText.setText("ОШИБКа");
                    resultText.setBackgroundColor(Color.RED);
                }
            }
        });
    }

}