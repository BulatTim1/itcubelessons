package com.example.testactivity.RecyclerViewTest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.testactivity.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView testRV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        testRV = findViewById(R.id.testRecyclerView);

        ArrayList<MyData> myArr = new ArrayList<MyData>();
        myArr.add(new MyData(1, "asdasf", false));
        myArr.add(new MyData(54, "qwesf", false));
        myArr.add(new MyData(43, "dbpsdf", true));
        myArr.add(new MyData(123, "eorgkw", false));

        RecyclerHelper rh = new RecyclerHelper(myArr);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        testRV.setLayoutManager(layoutManager);
        testRV.setAdapter(rh);
    }
}