package com.example.testactivity.TextGame;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.RecoverySystem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.testactivity.R;

public class testGame extends AppCompatActivity {

    TextView nameTV, statTV, storyTV;
    Story story;
    Character character;
    Button first, second, third;
    ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_game);
        nameTV = findViewById(R.id.nameTextView);
        character = new Character(getIntent().getStringExtra("name"));
        nameTV.setText("Приветствую, " + character.name);
        statTV = findViewById(R.id.statTextView);
        story = new Story();
        first = findViewById(R.id.firstAction);
        first.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                story.go(1);
                showStat();
            }
        });
        second = findViewById(R.id.seconAction);
        second.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                story.go(2);
                showStat();
            }
        });
        third = findViewById(R.id.thirdAction);
        third.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                story.go(3);
                showStat();
            }
        });
        storyTV = findViewById(R.id.storyTextView);
        showStat();
    }

    public void showStat(){
        character.A += story.current_situation.dA;
        character.K += story.current_situation.dK;
        character.R += story.current_situation.dR;
        storyTV.setText(story.current_situation.text);
        statTV.setText( "зп = " + character.A + "\nКарьера = " + character.K + "\nРепутация = " + character.R);
        if (story.current_situation.variantsText != null) {
            first.setText(story.current_situation.variantsText[0]);
            second.setText(story.current_situation.variantsText[1]);
            third.setText(story.current_situation.variantsText[2]);
        } else {
            first.setEnabled(false);
            second.setEnabled(false);
            third.setEnabled(false);
        }
    }

}