package com.example.testactivity.TextGame;

public class Situation {
    Situation[] direction;
    String text;
    String[] variantsText;
    int dK,dA,dR;
    public Situation ( String text, int variants, int dk,int da,int dr, String[] variantsText) {
        this.text=text;
        dK=dk;
        dA=da;
        dR=dr;
        direction=new Situation[variants];
        this.variantsText = variantsText;
    }
}
