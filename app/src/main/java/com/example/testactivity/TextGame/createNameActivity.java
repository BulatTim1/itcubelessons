package com.example.testactivity.TextGame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.testactivity.R;

public class createNameActivity extends AppCompatActivity {

    EditText nameEdit;
    Button startBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_name);
        nameEdit = findViewById(R.id.enterNameEdit);
        startBtn = findViewById(R.id.startBtn);

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nameEdit.getText().toString().equals("")){
                    Toast.makeText(createNameActivity.this, "Введите имя!", Toast.LENGTH_LONG).show();
                } else {
                    Intent startIntent = new Intent(createNameActivity.this, testGame.class);
                    startIntent.putExtra("name", nameEdit.getText().toString());
                    startActivity(startIntent);
                }
            }
        });

    }
}