package com.example.testactivity.DbForArthur;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.example.testactivity.R;

public class booksActiv extends AppCompatActivity {

    SQLiteDatabase db;
    DbHelper dbHelper;

    EditText name, authorId,price;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books2);
        dbHelper = new DbHelper(getApplicationContext(), "TestDB", null, 1);

        db = dbHelper.getReadableDatabase();

        name = findViewById(R.id.bookNameEdit);
        authorId = findViewById(R.id.authorIdEdit);
        price = findViewById(R.id.priceEdit);
        btn = findViewById(R.id.addBookBtn);


        btn.setOnClickListener(v->{
            ContentValues values = new ContentValues();
            values.put("name", name.getText().toString());
            values.put("authorId", Integer.valueOf(authorId.getText().toString()));
            values.put("price", Integer.valueOf(price.getText().toString()));
            db.insert("book", null, values);
            readData();
        });


    }
    public void readData(){
        Cursor c = db.rawQuery("SELECT * FROM book", null);
        while(c.moveToNext()){
            Log.d("MYTAG", c.getString(0) + " " + c.getString(1)
                    + " " + c.getString(2) + " " + c.getString(3));
            Cursor authorNameCursor = db.rawQuery("SELECT name FROM author WHERE id = ?", new String[]{c.getString(3)});
            authorNameCursor.moveToFirst();
            Log.d("MYTAG",authorNameCursor.getString(0));
        }

    }
}