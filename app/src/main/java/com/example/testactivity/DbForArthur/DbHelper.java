package com.example.testactivity.DbForArthur;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DbHelper extends SQLiteOpenHelper {
    public DbHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE book(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "name TEXT," +
                "price INTEGER," +
                "authorId INTEGER);");

        db.execSQL("CREATE TABLE author(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "name TEXT);");

        db.execSQL("INSERT INTO author(name) VALUES ('Gluhovsky');");
        db.execSQL("INSERT INTO author(name) VALUES ('Rihter');");

        db.execSQL("INSERT INTO book(name, price, authorId) VALUES('Metro2033', 500, 1)");
        db.execSQL("INSERT INTO book(name, price, authorId) VALUES('Metro2035', 750, 1)");
        db.execSQL("INSERT INTO book(name, price, authorId) VALUES('C# programming', 1250, 2)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
