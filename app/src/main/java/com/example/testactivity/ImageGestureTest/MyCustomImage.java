package com.example.testactivity.ImageGestureTest;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.MotionEventCompat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.testactivity.BirdGame.Game;
import com.example.testactivity.R;

import static android.view.MotionEvent.INVALID_POINTER_ID;

@SuppressLint({"AppCompatCustomView", "ViewConstructor"})
public class MyCustomImage extends ImageView {

    private ScaleGestureDetector mScaleGestureDetector;
    private float mScaleFactor = 1.0f;
    ImageView image;
    ImageView deleteImage;

    @SuppressLint("ClickableViewAccessibility")
    public MyCustomImage(@NonNull Context context, int imageId, ConstraintLayout layout) {
        super(context);
        final float[] dX = new float[1];
        final float[] dY = new float[1];
        deleteImage = new ImageView(context);
        deleteImage.setImageResource(R.drawable.ic_menu_manage);
        image = new ImageView(context);
        image.setImageResource(imageId);

        deleteImage.setOnTouchListener(new View.OnTouchListener(){

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                Log.d("MYTAG","removed");
                layout.removeView(image);
                layout.removeView(deleteImage);
                return true;
            }

        });

        image.setOnTouchListener((v, event) -> {
            if (event.getPointerCount() == 1) {
                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        dX[0] = v.getX() - event.getRawX();
                        dY[0] = v.getY() - event.getRawY();
                        break;

                    case MotionEvent.ACTION_MOVE:
                        v.animate()
                                .x(event.getRawX() + dX[0])
                                .y(event.getRawY() + dY[0])
                                .setDuration(0)
                                .start();
                        deleteImage.animate()
                                .x(event.getRawX() + dX[0])
                                .y(event.getRawY() + dY[0])
                                .setDuration(0)
                                .start();
                        break;
                    case MotionEvent.ACTION_UP:
                        break;
                    default:
                        return false;
                }
            }else{
                mScaleGestureDetector.onTouchEvent(event);
            }
            return true;
        });
        mScaleGestureDetector = new ScaleGestureDetector(getContext(), new ScaleListener());
        layout.addView(image);
        layout.addView(deleteImage);
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            Log.d("MYTAG", String.valueOf(mScaleFactor));
            mScaleFactor *= scaleGestureDetector.getScaleFactor();
            image.setScaleX(mScaleFactor);
            image.setScaleY(mScaleFactor);
            return true;
        }
    }
}