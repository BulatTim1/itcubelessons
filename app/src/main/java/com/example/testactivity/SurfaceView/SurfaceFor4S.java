package com.example.testactivity.SurfaceView;

import android.app.Notification;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.example.testactivity.R;

public class SurfaceFor4S extends SurfaceView implements SurfaceHolder.Callback {
    Context context;
    public SurfaceFor4S(Context context) {
        super(context);
        this.context = context;
        getHolder().addCallback(this);
    }

    MyThread4S t;

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder holder) {
        t = new MyThread4S(getContext(), holder);
        t.start();
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder holder) {

    }

    public class MyThread4S extends Thread{
        public SurfaceHolder holder;
        public boolean running = true;
        public int x, y;
        public int radius = 1;

        public MyThread4S(Context context, SurfaceHolder holder){
            this.holder = holder;
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        public void run() {
            super.run();
            Paint background = new Paint();
            background.setColor(Color.BLACK);
            Paint p = new Paint();
            p.setColor(Color.YELLOW);
            while (running){
                Canvas canvas = holder.lockCanvas();
                canvas.drawRect(0, 0, getWidth(), getHeight(), background);
                radius += 10;
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                holder.unlockCanvasAndPost(canvas);
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN){
            t.radius = 1;
            t.x = (int) event.getX();
            t.y = (int) event.getY();
        }
        return true;
    }
}
