package com.example.testactivity.SurfaceView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

import com.example.testactivity.R;
import com.example.testactivity.threads.ThreadExampleActivity;

public class MyView extends SurfaceView implements SurfaceHolder.Callback {
    MyThread thread;
    public MyView(Context context) {
        super(context);
        getHolder().addCallback(this);
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder holder) {
        thread = new MyThread(getContext(), holder);
        thread.start();
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder holder) {
        thread.interrupt();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        thread.setTowardPoint((int)event.getX(),(int)event.getY());
        thread.resetRadius();
        return false;
    }

    class MyThread extends Thread{
        private SurfaceHolder surfaceHolder;
        private volatile boolean running = true;
        private Paint backgroundPaint = new Paint();
        private Paint circlePaint = new Paint();
        private int towardPointX;
        private int towardPointY;
        private int radius = 0;
        {
                backgroundPaint.setColor(Color.BLUE);
                backgroundPaint.setStyle(Paint.Style.FILL);
                circlePaint.setColor(Color.YELLOW);
                circlePaint.setStyle(Paint.Style.FILL);
        }


        public MyThread(Context context, SurfaceHolder surfaceHolder){
            this.surfaceHolder = surfaceHolder;
        }

        @Override
        public void run() {
            super.run();
            int smileX = 0;
            int smileY = 0;
            while (running) {
                Canvas canvas = surfaceHolder.lockCanvas();
                if (canvas != null) {
                    try {
                        canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), backgroundPaint);
                        canvas.drawCircle(towardPointX, towardPointY, radius, circlePaint);
                        radius += 5;
                        sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }

        public void setTowardPoint(int x, int y) {
            towardPointX = x;
            towardPointY = y;
        }
        public void resetRadius(){
            radius = 0;
        }
    }
}
