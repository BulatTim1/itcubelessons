package com.example.testactivity.testBottomSheet;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.example.testactivity.R;

public class SheetActivity extends AppCompatActivity implements BottomSheetDialog.SheetListener {

    Button bottomBtn;
    TextView someText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shifting);
        bottomBtn = findViewById(R.id.opennerBtn);
        bottomBtn.setOnClickListener(v -> {
//            BottomSheetDialog bsd = new BottomSheetDialog();
//            bsd.show(getSupportFragmentManager(), "someTag");

        });
        someText = findViewById(R.id.someActionText);
    }

    @Override
    public void onClick(String text) {
        someText.setText(text);
    }
}