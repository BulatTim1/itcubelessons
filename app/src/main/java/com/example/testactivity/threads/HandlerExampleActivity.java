 package com.example.testactivity.threads;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;

import com.example.testactivity.R;

public class HandlerExampleActivity extends AppCompatActivity {

    Handler h;
    TextView t1;
    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handler_example);
        t1 = findViewById(R.id.textView3);

        SendMessageClass s1 = new SendMessageClass("1");
        SendMessageClass s2 = new SendMessageClass("2");

        h = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {
                t1.append(String.valueOf(msg.obj));
                i++;
                if (i == 5){
                    s2.start();
                }
                return false;
            }
        });
        s1.start();
    }

    public class SendMessageClass extends Thread{
        String msg;
        SendMessageClass(String msg){
            this.msg = msg;
        }
        @Override
        public void run() {
            super.run();
            while (true) {
                Message hmsg = new Message();
                hmsg.obj = msg;
                h.sendMessage(hmsg);
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}